QUnit.test('Testing calculateArea function with several sets of inputs', function (assert) {
    assert.equal(vfirst("aeiou"), 5, 'Tested with characters on first name');
    assert.equal(vlast("adgiu"), 3, 'Tested with charcters on last name');
        //throws( block                                    [, expected ] [, message ] ) 
    assert.throws(function () { vfirst(""); }, /The given argument is not a valid name/, 'Passing in null correctly raises an Error');
        //throws( block                                    [, expected ] [, message ] ) 
    assert.throws(function () { vfirst("a345"); }, /The given argument is not a valid name/, 'Passing in a string with numbers raises an Error');
    assert.throws(function () { vfirst("a.@#"); }, /The given argument is not a valid name/, 'Passing in a string with special characters raises an Error');
});